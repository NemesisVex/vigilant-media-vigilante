<?php
global $config_url_base;

$config_url_base = array();
switch (ENVIRONMENT) {
    case 'dev':
    case 'development':
		$config_url_base['to_vigilantmedia'] = 'https://dev.vigilantmedia.com';
		$config_url_base['to_vigilante'] = 'https://dev.vigilante.vigilantmedia.com';
		$config_url_base['to_sandbox'] = 'https://sandbox.vigilantmedia.com';
		$config_url_base['to_mt'] = 'https://dev.mt.vigilantmedia.com';
		$config_url_base['to_wp'] = 'https://wp.vigilantmedia.com';
		$config_url_base['to_gregbueno'] = 'https://dev.gregbueno.com';
		$config_url_base['to_archive'] = 'https://archive.wp-dev.musicwhore.org';
		$config_url_base['to_musicwhore'] = 'https://wp-dev.musicwhore.org';
		$config_url_base['to_reviews'] = 'https://musicwhore.wp.vigilantmedia.com';
		$config_url_base['to_filmwhore'] = 'https://film.wp-dev.musicwhore.org';
		$config_url_base['to_tvwhore'] = 'https://tv.wp-dev.musicwhore.org';
		$config_url_base['to_journalcon'] = 'https://dev.journalcon.austin-stories.com';
		$config_url_base['to_austinstories'] = 'https://dev.austin-stories.com';
		$config_url_base['to_ddn'] = 'https://dev.duran-duran.net';
		$config_url_base['to_eponymous4'] = 'https://dev.eponymous4.com';
		$config_url_base['to_penziaswilson'] = 'https://dev.penziasandwilson.com';
		$config_url_base['to_minedagap'] = 'https://dev.minedagap.com';
		$config_url_base['to_observant'] = 'https://dev.observantrecords.com';
		$config_url_base['to_observantadmin'] = 'https://dev.admin.observantrecords.com';
		$config_url_base['to_observantshop'] = 'https://dev.shop.observantrecords.com';
		$config_url_base['to_shinkyokuadvocacy'] = 'https://dev.shinkyokuadvocacy.com';
		$config_url_base['to_emptyensemble'] = 'https://dev.emptyensemble.com';
		break;
    case 'test':
    case 'testing':
	case 'staging':
		$config_url_base['to_vigilantmedia'] = 'https://test.vigilantmedia.com';
		$config_url_base['to_vigilante'] = 'https://test.vigilante.vigilantmedia.com';
		$config_url_base['to_sandbox'] = 'https://sandbox.vigilantmedia.com';
		$config_url_base['to_mt'] = 'https://test.mt.vigilantmedia.com';
		$config_url_base['to_wp'] = 'https://wp-test.vigilantmedia.com';
		$config_url_base['to_gregbueno'] = 'https://test.gregbueno.com';
		$config_url_base['to_archive'] = 'https://archive.wp-test.musicwhore.org';
		$config_url_base['to_musicwhore'] = 'https://wp-test.musicwhore.org';
		$config_url_base['to_reviews'] = 'https://musicwhore.wp-test.vigilantmedia.com';
		$config_url_base['to_filmwhore'] = 'https://film.wp-test.musicwhore.org';
		$config_url_base['to_tvwhore'] = 'https://tv.wp-test.musicwhore.org';
		$config_url_base['to_journalcon'] = 'https://test.journalcon.austin-stories.com';
		$config_url_base['to_austinstories'] = 'https://test.austin-stories.com';
		$config_url_base['to_ddn'] = 'https://test.duran-duran.net';
		$config_url_base['to_eponymous4'] = 'https://test.eponymous4.com';
		$config_url_base['to_emptyensemble'] = 'https://test.emptyensemble.com';
		$config_url_base['to_penziaswilson'] = 'https://test.penziasandwilson.com';
		$config_url_base['to_minedagap'] = 'https://test.minedagap.com';
		$config_url_base['to_observant'] = 'https://test.observantrecords.com';
		$config_url_base['to_observantadmin'] = 'https://test.admin.observantrecords.com';
		$config_url_base['to_observantshop'] = 'https://test.shop.observantrecords.com';
		$config_url_base['to_shinkyokuadvocacy'] = 'https://test.shinkyokuadvocacy.com';
		break;
	case 'prod':
	case 'production':
    default:
		$config_url_base['to_vigilantmedia'] = 'https://vigilantmedia.com';
		$config_url_base['to_vigilante'] = 'https://vigilante.vigilantmedia.com';
		$config_url_base['to_sandbox'] = 'https://sandbox.vigilantmedia.com';
		$config_url_base['to_mt'] = 'https://mt.vigilantmedia.com';
		$config_url_base['to_wp'] = 'https://blog.vigilantmedia.com';
		$config_url_base['to_gregbueno'] = 'https://gregbueno.com';
		$config_url_base['to_archive'] = 'https://archive.musicwhore.org';
		$config_url_base['to_musicwhore'] = 'https://musicwhore.org';
		$config_url_base['to_reviews'] = 'https://reviews.musicwhore.org';
		$config_url_base['to_filmwhore'] = 'https://filmwhore.org';
		$config_url_base['to_tvwhore'] = 'https://tvwhore.org';
		$config_url_base['to_journalcon'] = 'https://journalcon.austin-stories.com';
		$config_url_base['to_austinstories'] = 'https://austin-stories.com';
		$config_url_base['to_ddn'] = 'https://duran-duran.net';
		$config_url_base['to_eponymous4'] = 'https://eponymous4.com';
		$config_url_base['to_emptyensemble'] = 'https://emptyensemble.com';
		$config_url_base['to_penziaswilson'] = 'https://penziasandwilson.com';
		$config_url_base['to_minedagap'] = 'https://minedagap.com';
		$config_url_base['to_observant'] = 'https://observantrecords.com';
		$config_url_base['to_observantadmin'] = 'https://admin.observantrecords.com';
		$config_url_base['to_observantshop'] = 'https://shop.observantrecords.com';
		$config_url_base['to_shinkyokuadvocacy'] = 'https://shinkyokuadvocacy.com';
		break;
}
